const hpDescription = 'This laptop is perfect for businesses as it is very powerful, and gives the employees' +
    ' a good working experience.'
const hpFeatures = ['15.6 inches', ' AMD Ryzen 5', ' 256 GB storage', ' 16 GB RAM', ' B&O speakers']
const lenovoDescription = 'This laptop is perfect for personal use. You can turn the screen 360 degrees around' +
    'and use it as a tablet. With touch screen of course!'
const lenovoFeatures = ['14.0 inches', ' Intel Core i5', ' 512 GB storage', ' 8 GB RAM', ' Touch screen']
const macDescription = 'This laptop is perfect for students and workers who works with creative stuff. It has a' +
    'stunning battery life and har become one of the greatest laptops in the marked.'
const macFeatures = ['15.6 inches', ' Intel Core i5', ' 512 GB storage', ' 16 GB RAM', ' MacOS']
const acerDescription = 'This computer is perfect for gamers. With high-quality graphic and super-fast processor ' +
    'it is a dream to play your games on!'
const acerFeatures = ['15.6 inches', ' Intel Core i7', ' 512 GB storage', ' 16 GB RAM', ' Nvidia GeForce GTX 1660 Ti']

const laptopList = [
    new Laptop(1,'HP Elitebook', hpDescription, hpFeatures,15000, 'images/hpElitebook.jpg'),
    new Laptop(2,'Lenovo Yoga', lenovoDescription, lenovoFeatures, 8999, 'images/lenovoYoga.jpg'),
    new Laptop(3,'Macbook Pro', macDescription, macFeatures, 20000, 'images/macbookPro.jpg'),
    new Laptop(4,'Acer Nitro', acerDescription, acerFeatures, 11000, 'images/acerNitro.jpg'),
]

function Laptop (id, name, description, features, price, image) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.features = features
    this.price = price;
    this.image = image;
}

const laptopSelected = document.getElementById("laptops");
const laptopSel = document.getElementById("laptop")
const featureSelected = document.getElementById("features");
const descriptionSelected = document.getElementById("description");
const priceSelected = document.getElementById("price");
const imageSelected = document.getElementById("image");

//Inserts the laptop name into the dropdown list
for (const laptop of laptopList){
    //creating a new <option>
    const laptopOption = document.createElement('option')
    laptopOption.value = laptop.id
    laptopOption.innerText = laptop.name
    laptopSelected.appendChild(laptopOption)
}
let laptopName
let laptopPrice
const info = document.getElementById('info')
info.style.visibility = 'hidden'

//Handles the changes which should be made when changing between the laptops
//Changes the features, image, description and price based on what laptop is chosed
laptopSelected.addEventListener('change', function () {
    const value = Number(this.value)
    const selectedLaptop = laptopList.find(function (laptop) {
        return laptop.id === value;
    })
    laptopName = selectedLaptop.name;
    laptopSel.innerText = laptopName;
    featureSelected.innerText = selectedLaptop.features.toString();
    descriptionSelected.innerText = selectedLaptop.description;
    laptopPrice = selectedLaptop.price
    priceSelected.innerText = laptopPrice;
    imageSelected.src = selectedLaptop.image;
    console.log(document.getElementById("price"))
    console.log(value)
    showInfoSection()
})

//Displays the info section of the computer, when a computer is selected
showInfoSection = () => info.style.visibility = 'visible'



//Economy part

const bal = document.getElementById("balance")
const loanAmount = document.getElementById("loan")
const loanButton = document.getElementById("loanBtn")
const payLoanButton = document.getElementById('payLoanButton')

//Set the start balance
let balance = 3000;
let amountLoaned = 0;
bal.innerText = balance;
loanAmount.innerText = amountLoaned;

payLoanButton.style.visibility = 'hidden'

//Handles the request for a loan and check if it should be granted or not
//Updates the current stats, if the loan is granted
loanButton.addEventListener('click', () => {
    if (amountLoaned === 0){
        let amountRequested = Number(prompt("Enter loan amount"))
        //Check if valid input
        if (Number.isInteger(amountRequested) && amountRequested > 0){
            //Check if the loan should be granted. If you already have a loan, then amountLoaned != 0
            if (balance * 2 >= amountRequested && amountLoaned === 0){
                balance += amountRequested;
                amountLoaned = amountRequested;
                alert(`Your loan of ${amountRequested} kr was granted!`)
                showLoanButton()
            } else {
                alert(`The amount you requested, was too high!`)
            }
            bal.innerText = balance;
            loanAmount.innerText = amountLoaned;
        } else {
            alert('Invalid input!')
        }
    } else {
        alert(`You already have a loan. Pay it back first`)
    }
})


//Work part

const btn = document.querySelector(".workButton")
const bankBtn = document.querySelector(".bankButton")
const payLoanBtn = document.querySelector(".payLoanBtn")
const workbtn = document.getElementById('workBtn')

let payBalance = 0
workbtn.innerText = payBalance;
//Updates the balance with 100 for each time clicked
btn.addEventListener('click', () => {
    payBalance += 100
    workbtn.innerText = payBalance;
})

//Handles the bank button. The pay balance is transferred to 90% personal balance, and 10% to a possibly loan
bankBtn.addEventListener('click', () => {
    let remaining;
    if (amountLoaned > 0){
        //If 10% of the pay balance is bigger then the remaining loan, then the rest is transferred to personal balance
        if (payBalance * 0.1 > amountLoaned){
            remaining = amountLoaned - (payBalance * 0.1);
            balance += payBalance * 0.9 - remaining             //Adding remaining with -, because remaining is negative
            amountLoaned = 0
        } else {
            amountLoaned = amountLoaned - (payBalance * 0.1);
            balance += payBalance * 0.9
        }
    } else {
        balance += payBalance;
    }
    payBalance = 0;
    loanAmount.innerText = amountLoaned
    workbtn.innerText = payBalance;
    bal.innerText = balance;
    showLoanButton()
})

//Pays all from work pay to loan. If the balance is higher then the loan, then the rest is staying at pay balance
payLoanBtn.addEventListener('click', () => {
    let extra
    if (amountLoaned > 0){
        if (amountLoaned - payBalance < 0){
            extra = payBalance - amountLoaned;
            amountLoaned = 0;
            payBalance = extra;
        }else {
            amountLoaned -= payBalance;
            payBalance = 0;
        }
    }
    loanAmount.innerText = amountLoaned
    workbtn.innerText = payBalance;
    showLoanButton()
})

//Hide and shows the loan button based on you have loan or not
showLoanButton = () => {
    if (amountLoaned > 0){
        payLoanButton.style.visibility = 'visible'
    } else {
        payLoanButton.style.visibility = 'hidden'
    }
}


// Buy part

const buybtn = document.querySelector('.buyButton')

//Buys the new laptop if it is enough money on the balance
buybtn.addEventListener('click',  () => {
    console.log(laptopName)
    if (balance >= laptopPrice){
        alert(`Congratulations! You are now the owner of a new ${laptopName}!`)
        balance -= laptopPrice;
        bal.innerText = balance;
    } else {
        alert(`You do not have enough money to buy ${laptopName}`)
    }
})
