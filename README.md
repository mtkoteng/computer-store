# Computer store
## The task
The goal of this assignment was to create a simple computer store,
that also handles work payment and loan. A website with plain
JavaScript was used to create this application.
## Run application
Simply run the index.html
## The solution
### Styling
For styling the elements, CSS has been used without framework.
For the hierarchy of the site, CSS Flexbox has been used.
### Functionality
#### Personal
In the personal section (with the name Ola Normann) your personal
balance and your loan (if you have one) is showed. You can ask for
a loan by clicking the button "Get a loan". If you do not already have
a loan, then you will be able to ask for a loan amount. If the requested 
loan is not bigger than twice the balance, then it is granted and
your balance and loan gets updated.
#### Work
In this section you can click on "Work" to generate income. This
will update your pay balance with 100 NOK for each time you press
the button. By clicking on "Bank", your money will be transferred to
your personal account. If you have loan, then 10% of your
pay balance will be transferred to pay down the loan, the remaining
90% will go to your personal balance. If you have a loan, you can press the
loan button, to pay the total amount of your pay balance, on the loan.
If the pay balance is bigger than the remaining loan, then the rest will still
be placed in the pay balance.
#### Laptops
In this section you can choose between the different laptops.
When selecting a laptop, the features of that laptop will be shown
underneath. The info section will also then be displayed.
#### Info section
IMPORTANT! This section will not be visible before selecting a
laptop.

This section shows the information about the selected laptop.
It gives you picture, title, description and price of the selected laptop.
You are then able to buy the laptop, if you have enough money
on your personal balance.